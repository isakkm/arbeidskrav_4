package no.ntnu.idatt2001.oblig4.cardgame.View;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import no.ntnu.idatt2001.oblig4.cardgame.Model.DeckOfCards;
import no.ntnu.idatt2001.oblig4.cardgame.Model.HandOfCards;
import no.ntnu.idatt2001.oblig4.cardgame.Model.PlayingCard;


public class ApplicationFront extends Application {

  private Stage stage;
  private final int NUM_CARDS = 5;

  @Override
  public void start(Stage stage) throws Exception {
    this.stage = stage;
    stage.setScene(openingScreen());
    stage.show();
  }

  public Scene openingScreen() {
    Text title = newText("Card Game", 50, true, 175, 100);
    Button newGameButton = newGameButton("New Game", 200, 200, "black", "white", 200, 50, 20);
    Button exitGameButton = newGameButton("Exit Game", 200, 300, "black", "white", 200, 50, 20);

    newGameButton.setOnAction(e -> stage.setScene(gameScreen()));
    exitGameButton.setOnAction(e -> stage.close());

    Group root = new Group(title, newGameButton, exitGameButton);
    Scene scene = new Scene(root, 600, 400, Color.DARKSEAGREEN);
    stage.setScene(scene);
    return scene;
  }

  public Scene gameScreen() {

    DeckOfCards deck = new DeckOfCards();
    HandOfCards hand = new HandOfCards();
    deck.shuffle();

    Button dealHand = newGameButton("Deal Hand", 180, 300, "black", "white", 200, 50, 20);
    Button checkHand = newGameButton("Check Hand", 425, 300, "black", "white", 200, 50, 20);

    HBox handField = new HBox(10);
    for (int i = 0; i < NUM_CARDS; i++) {
      Image image = new Image("file:src/main/resources/back1.png");
      ImageView cardBack = new ImageView(image);
      cardBack.setFitWidth(75);
      cardBack.setFitHeight(90);
      handField.getChildren().add(cardBack);
    }

    handField.setSpacing(20);
    handField.setLayoutX(175);
    handField.setLayoutY(200);

    HBox deckField = new HBox(10);
    Image deckCardBack = new Image("file:src/main/resources/back1.png");
    ImageView deckCardBackView = new ImageView(deckCardBack);
    deckCardBackView.setFitWidth(75);
    deckCardBackView.setFitHeight(90);
    deckField.getChildren().add(deckCardBackView);

    deckField.setLayoutX(365);
    deckField.setLayoutY(90);

    TextField hearts = newTextField("", 180, 400, "black", "white", 200, 50, 20);
    TextField flush = newTextField("", 425, 400, "black", "white", 200, 50, 20);
    TextField queen = newTextField("", 180, 500, "black", "white", 250, 50, 20);
    TextField sum = newTextField("", 475, 500, "black", "white", 150, 50, 20);

    hearts.setEditable(false);
    flush.setEditable(false);
    queen.setEditable(false);
    sum.setEditable(false);

    dealHand.setOnAction(e -> {
      hand.setHand(deck.dealHand(NUM_CARDS));
      handField.getChildren().clear();
      for (PlayingCard card : hand.getHand()) {
        Image image = new Image(
            "file:src/main/resources/" + card.getSuit() + card.getFace() + ".png");
        ImageView cardFront = new ImageView(image);
        cardFront.setFitWidth(75);
        cardFront.setFitHeight(90);
        handField.getChildren().add(cardFront);
      }
    });

    checkHand.setOnAction(e -> {
      hearts.setText(hand.hasHearts());
      flush.setText(hand.isFlush());
      queen.setText(hand.hasQueenOfSpades());
      sum.setText(String.valueOf(hand.calculateSumOfHand()));
    });

    Group root = new Group(dealHand, checkHand, handField, deckField, hearts, flush, queen, sum);
    Scene scene = new Scene(root, 800, 600, Color.DARKSEAGREEN);
    stage.setScene(scene);
    return scene;
  }


  public Image newImage(String path, int x, int y, int width, int height) {
    Image image = new Image(path);
    ImageView imageView = new ImageView(image);
    imageView.setX(x);
    imageView.setY(y);
    imageView.setFitWidth(width);
    imageView.setFitHeight(height);
    imageView.setPreserveRatio(true);
    return image;
  }

  public Button newGameButton(String text, int x, int y, String borderColor,
      String backgroundColor, int width, int height, int fontSize) {
    Button button = new Button(text);
    button.setLayoutX(x);
    button.setLayoutY(y);
    button.setStyle(setStyleString(borderColor, backgroundColor, width, height, fontSize));
    button.setOnMouseEntered(
        e -> button.setStyle(setStyleString(borderColor, "grey", width, height, fontSize)));
    button.setOnMouseExited(e -> button.setStyle(
        setStyleString(borderColor, backgroundColor, width, height, fontSize)));
    return button;
  }

  public TextField newTextField(String promptText, int x, int y, String borderColor,
      String backgroundColor, int width, int height, int fontSize) {
    TextField textField = new TextField();
    textField.setPromptText(promptText);
    textField.setLayoutX(x);
    textField.setLayoutY(y);
    textField.setStyle(setStyleString(borderColor, backgroundColor, width, height, fontSize));
    return textField;
  }

  public Text newText(String title, int size, boolean underline, int x, int y) {
    Text text = new Text(title);
    text.setFont(Font.font("Times New Roman", FontWeight.BOLD, size));
    text.setUnderline(underline);
    text.setX(x);
    text.setY(y);
    return text;
  }

  public String setStyleString(String borderColor,
      String backgroundColor, int width, int height, int fontSize) {
    return "-fx-border-color: " + borderColor + ";" +
        "-fx-background-color: " + backgroundColor + ";" +
        "-fx-pref-width: " + width + ";" +
        "-fx-pref-height: " + height + ";" +
        "-fx-font-size: " + fontSize + "px;";
  }

  public static void main(String[] args) {
    launch();
  }
}
