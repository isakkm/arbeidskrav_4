package no.ntnu.idatt2001.oblig4.cardgame.Model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DeckOfCards {

  private final List<PlayingCard> deck = new ArrayList<>();

  /**
   * Constructor for DeckOfCards.
   * Creates a deck of 52 cards.
   */
  public DeckOfCards() {
    char[] suit = {'S', 'H', 'D', 'C'};
    int[] face = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
    for (char s : suit) {
      for (int f : face) {
        deck.add(new PlayingCard(s, f));
      }
    }
  }

  /**
   * Getter for the deck.
   * @return the deck
   */
  public List<PlayingCard> getDeck() {
    return deck;
  }

  /**
   * Deals a hand of n cards.
   *
   * @param n the number of cards to deal
   *          n must be between 0 and 52
   * @return a list of PlayingCards
   */
  public List<PlayingCard> dealHand(int n) throws IllegalArgumentException {
    if (n > deck.size()) {
      throw new IllegalArgumentException("You can't deal more cards than there are in the deck");
    }
    if (n < 1) {
      throw new IllegalArgumentException("You can't deal a negative number of cards");
    }

    List<PlayingCard> hand = new ArrayList<>();
    List<PlayingCard> deck = new ArrayList<>(this.deck);

    Random random = new Random();
    for (int i = 0; i < n; i++) {
      int index = random.nextInt(deck.size());
      hand.add(deck.get(index));
      deck.remove(index);
    }
    return hand;
  }

  /**
   * Shuffles the deck.
   */
  public void shuffle() {
    List<PlayingCard> deck = new ArrayList<>(this.deck);
    List<PlayingCard> shuffledDeck = new ArrayList<>();
    Random random = new Random();
    for (int i = 0; i < 52; i++) {
      int index = random.nextInt(deck.size());
      shuffledDeck.add(deck.get(index));
      deck.remove(index);
    }
    this.deck.clear();
    this.deck.addAll(shuffledDeck);
  }
}
