package no.ntnu.idatt2001.oblig4.cardgame.Model;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class HandOfCards {

  private List<PlayingCard> hand;

  /**
   * Constructor for HandOfCards. Creates an empty hand.
   */
  public HandOfCards() {
    hand = new ArrayList<>();
  }

  /**
   * Constructor for HandOfCards. Creates a hand with the given list of cards.
   * The hand cannot be null or empty.
   *
   * @param hand the list of cards to be added to the hand.
   */
  public HandOfCards(List<PlayingCard> hand) {
    if (hand == null) {
      throw new IllegalArgumentException("The hand cannot be null.");
    }
    if (hand.isEmpty()) {
      throw new IllegalArgumentException("The hand cannot be empty.");
    }
    this.hand = hand;
  }

  /**
   * Getter for the hand.
   *
   * @return the hand
   */
  public List<PlayingCard> getHand() {
    return hand;
  }

  /**
   * Setter for the hand.
   *
   * @param hand the hand to be set
   */
  public void setHand(List<PlayingCard> hand) {
    this.hand = hand;
  }

  /**
   * Calculates the sums of the cards in the hand.
   *
   * @return the sum of the cards in the hand
   */
  public int calculateSumOfHand() {
    return hand.stream().mapToInt(PlayingCard::getFace).sum();
  }



  /**
   * Checks for hearts in the hand.
   *
   * @return Suits and faces of the hearts in the hand, or "No Hearts" if there are no hearts in the
   * hand.
   */
  public String hasHearts() {
    return hand.stream()
        .filter(card -> card.getSuit() == 'H')
        .map(PlayingCard::getAsString)
        .collect(Collectors.collectingAndThen(Collectors.joining(" "),
            str -> str.isEmpty() ? "No Hearts" : str.trim()));
  }

  /**
   * Checks for a flush in the hand.
   *
   * @return "Flush" if there is a flush in the hand, or "Not a flush" if there is no flush in the
   * hand.
   */
  public String isFlush() {
    return hand.stream()
        .map(PlayingCard::getSuit)
        .distinct()
        .count() == 1 ? "Flush" : "Not a flush";
  }

  /**
   * Checks for if the hand contains a queen of spades.
   *
   * @return "Queen of Spades in hand" if the hand contains a queen of spades, or "No Queen of Spades"
   */
  public String hasQueenOfSpades() {
    return hand.stream()
        .filter(card -> card.getSuit() == 'S' && card.getFace() == 12)
        .map(PlayingCard::getAsString)
        .collect(Collectors.collectingAndThen(Collectors.joining(" "),
            str -> str.isEmpty() ? "No Queen of Spades" : "Queen of Spades in hand"));
  }
}
