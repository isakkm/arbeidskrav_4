package no.ntnu.idatt2001.oblig4.cardgame.Model;

public class Main {

  public static void main(String[] args) {
    DeckOfCards deckOfCards = new DeckOfCards();
    HandOfCards handOfCards;
    StringBuilder hand = new StringBuilder();
    for (int i = 0; i < 10; i++) {
      handOfCards = new HandOfCards(deckOfCards.dealHand(5));
      for (PlayingCard card : handOfCards.getHand()) {
        hand.append(card.getAsString()).append(" ");
      }
      System.out.println(hand);
      hand = new StringBuilder();
    }
  }

}