package no.ntnu.idatt2001.oblig4.cardgame.Model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class DeckOfCardsTest {

@Nested
class TestConstructor {

    @Test
    public void testConstructorWithoutParameter() {
      DeckOfCards deckOfCards = new DeckOfCards();
      assertEquals(52, deckOfCards.getDeck().size());
    }
}

  @Nested
  class TestMethods {

    @Test
    public void testGetDeck() {
      DeckOfCards deckOfCards = new DeckOfCards();
      assertEquals(52, deckOfCards.getDeck().size());
    }

    @Test
    public void testDealHand() {
      DeckOfCards deckOfCards = new DeckOfCards();
      assertEquals(5, deckOfCards.dealHand(5).size());
    }

    @Test
    public void testDealHandWithInvalidParameter() {
      DeckOfCards deckOfCards = new DeckOfCards();
      assertThrows(IllegalArgumentException.class, () -> deckOfCards.dealHand(53));
      assertThrows(IllegalArgumentException.class, () -> deckOfCards.dealHand(0));
    }
  }
}