package no.ntnu.idatt2001.oblig4.cardgame.Model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

public class HandOfCardsTest {

  @Nested
  class TestConstructor {

    private List<PlayingCard> handOfCards;

    @Test
    public void testConstructorWithoutParameter() {
      DeckOfCards deckOfCards = new DeckOfCards();
      handOfCards = deckOfCards.dealHand(5);
      HandOfCards handOfCards1 = new HandOfCards();
      handOfCards1.setHand(handOfCards);
      assertEquals(5, handOfCards.size());
      assertFalse(handOfCards1.getHand().isEmpty());
    }

    @Test
    public void testConstructorWithParameter() {
      DeckOfCards deckOfCards = new DeckOfCards();
      handOfCards = deckOfCards.dealHand(5);
      HandOfCards handOfCards1 = new HandOfCards(handOfCards);
      assertEquals(5, handOfCards1.getHand().size());
      assertEquals(handOfCards, handOfCards1.getHand());
    }

    @Test
    public void testConstructorWithNullParameter() {
      assertThrows(IllegalArgumentException.class, () -> new HandOfCards(null));
    }

    @Test
    public void testConstructorWithEmptyParameter() {
      handOfCards = new ArrayList<>();
      assertThrows(IllegalArgumentException.class, () -> new HandOfCards(handOfCards));
    }
  }

  @Nested
  class TestMethods {

    @Test
    public void testGetHand() {
      DeckOfCards deckOfCards = new DeckOfCards();
      List<PlayingCard> handOfCards = deckOfCards.dealHand(5);
      HandOfCards handOfCards1 = new HandOfCards(handOfCards);
      assertEquals(handOfCards, handOfCards1.getHand());
    }

    @Test
    public void testSetHand() {
      DeckOfCards deckOfCards = new DeckOfCards();
      List<PlayingCard> handOfCards = deckOfCards.dealHand(5);
      HandOfCards handOfCards1 = new HandOfCards();
      handOfCards1.setHand(handOfCards);
      assertEquals(handOfCards, handOfCards1.getHand());
    }

    @Test
    public void testCalculateSumOfHand() {
      DeckOfCards deckOfCards = new DeckOfCards();
      List<PlayingCard> handOfCards = deckOfCards.dealHand(5);
      HandOfCards handOfCards1 = new HandOfCards(handOfCards);
      int sum = 0;
      for (PlayingCard card : handOfCards) {
        sum += card.getFace();
      }
      assertEquals(sum, handOfCards1.calculateSumOfHand());
    }

    @Test
    public void testHasHearts() {
      List<PlayingCard> handOfCards = new ArrayList<>();
      handOfCards.add(new PlayingCard('H', 3));
      handOfCards.add(new PlayingCard('S', 3));
      handOfCards.add(new PlayingCard('D', 3));
      handOfCards.add(new PlayingCard('C', 3));
      handOfCards.add(new PlayingCard('H', 4));
      HandOfCards handOfCards1 = new HandOfCards(handOfCards);

      assertEquals("H3 H4", handOfCards1.hasHearts());
    }

    @Test
    public void testHasNoHearts() {
      List<PlayingCard> handOfCards = new ArrayList<>();
      handOfCards.add(new PlayingCard('S', 3));
      handOfCards.add(new PlayingCard('D', 3));
      handOfCards.add(new PlayingCard('C', 3));
      handOfCards.add(new PlayingCard('S', 4));
      handOfCards.add(new PlayingCard('D', 4));
      HandOfCards handOfCards1 = new HandOfCards(handOfCards);

      assertEquals("No Hearts", handOfCards1.hasHearts());
    }

    @Test
    public void testIsFlush() {
      List<PlayingCard> handOfCards = new ArrayList<>();
      handOfCards.add(new PlayingCard('H', 3));
      handOfCards.add(new PlayingCard('H', 4));
      handOfCards.add(new PlayingCard('H', 5));
      handOfCards.add(new PlayingCard('H', 6));
      handOfCards.add(new PlayingCard('H', 7));
      HandOfCards handOfCards1 = new HandOfCards(handOfCards);

      assertEquals("Flush", handOfCards1.isFlush());
    }

    @Test
    public void testIsNotFlush() {
      List<PlayingCard> handOfCards = new ArrayList<>();
      handOfCards.add(new PlayingCard('H', 3));
      handOfCards.add(new PlayingCard('H', 4));
      handOfCards.add(new PlayingCard('H', 5));
      handOfCards.add(new PlayingCard('H', 6));
      handOfCards.add(new PlayingCard('S', 7));
      HandOfCards handOfCards1 = new HandOfCards(handOfCards);

      assertEquals("Not a flush", handOfCards1.isFlush());
    }

    @Test
    public void testHasQueenOfSpades() {
      List<PlayingCard> handOfCards = new ArrayList<>();
      handOfCards.add(new PlayingCard('H', 3));
      handOfCards.add(new PlayingCard('H', 4));
      handOfCards.add(new PlayingCard('H', 5));
      handOfCards.add(new PlayingCard('H', 6));
      handOfCards.add(new PlayingCard('S', 12));
      HandOfCards handOfCards1 = new HandOfCards(handOfCards);

      assertEquals("Queen of Spades in hand", handOfCards1.hasQueenOfSpades());
    }
  }
}
