package no.ntnu.idatt2001.oblig4.cardgame.Model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

class PlayingCardTest {

  @Nested
  class TestConstructor {

    @Test
    public void testConstructorWithValidParameters() {
      PlayingCard playingCard = new PlayingCard('C', 12);
      assertEquals('C', playingCard.getSuit());
      assertEquals(12, playingCard.getFace());
    }

    @Test
    public void testConstructorWithInvalidSuitParameter() {
      assertThrows(IllegalArgumentException.class, () -> new PlayingCard('1', 12));
    }

    @Test
    public void testConstructorWithInvalidFaceParameter() {
      assertThrows(IllegalArgumentException.class, () -> new PlayingCard('S', 14));
    }
  }

  @Nested
  class TestMethods {

    @Test
    public void testGetSuit() {
      PlayingCard playingCard = new PlayingCard('C', 12);
      assertEquals('C', playingCard.getSuit());
    }

    @Test
    public void testGetFace() {
      PlayingCard playingCard = new PlayingCard('C', 12);
      assertEquals(12, playingCard.getFace());
    }

    @Test
    public void testToString() {
      PlayingCard playingCard = new PlayingCard('C', 12);
      assertEquals("C12", playingCard.getAsString());
    }
  }

}